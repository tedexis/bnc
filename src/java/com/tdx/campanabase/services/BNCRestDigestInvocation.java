/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tdx.campanabase.services;

import com.tdx.campaigntools.objects.SMSRequest;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import javax.net.ssl.HttpsURLConnection;
import org.apache.commons.codec.digest.DigestUtils;

/**
 *
 * @author jalfonzo
 */
public class BNCRestDigestInvocation {

    static boolean debugMode = false;

    public static void printDebugMessage(String message) {
        if (debugMode) {
            printMessage(message);
        }
    }

    public static void printMessage(String message) {
        System.out.println("Thread:" + Thread.currentThread().getId() + " BNC P2P: " + message);
    }

    public static boolean validarMensaje(String msg, String regex) {
        Pattern pattern = null;
        Matcher matcher = null;
        pattern = Pattern.compile(regex);
        matcher = pattern.matcher(msg);
        return matcher.find();
    }

    private static Map<String, String> getAuthenticateParameters(Map<String, List<String>> map) {
        Map<String, String> result = new HashMap<>();
        List<String> h = map.get("WWW-Authenticate");
        String auth = h.get(0);
        printDebugMessage("DigestHeader = " + auth);
        String[] authar = auth.replace("Digest", "").replace("\"", "").split(",");
        for (int i = 0; i < authar.length; i++) {
            String[] params = authar[i].split("=");
            result.put(params[0].trim(), params[1].trim());
        }
        return result;
    }

    public static InputStream createRestRequest(String endpoint, String uri, String HttpMethod, String HeaderUsername, String HaderPassword, String input) throws Exception {
        String nc = "00000001";
        String cnonce = "kbxzfbbv";
        URL url = new URL(String.valueOf(endpoint) + uri);
        HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
        Map<String, List<String>> map = conn.getHeaderFields();
        Map<String, String> authenticateParameters = getAuthenticateParameters(map);
        String realm = authenticateParameters.get("realm");
        String qop = authenticateParameters.get("qop");
        String opaque = authenticateParameters.get("opaque");
        String nonce = authenticateParameters.get("nonce");
        String algorithm = authenticateParameters.get("algorithm");
        String hasha1 = DigestUtils.md5Hex(String.valueOf(HeaderUsername) + ":" + realm + ":" + HaderPassword);
        if (algorithm.equalsIgnoreCase("MD5-sess")) {
            hasha1 = DigestUtils.md5Hex(String.valueOf(DigestUtils.md5Hex(String.valueOf(HeaderUsername) + ":" + realm + ":" + HaderPassword)) + ":" + nonce + ":" + cnonce);
        }
        String hasha2 = DigestUtils.md5Hex(String.valueOf(HttpMethod) + ":" + uri);
        if (qop.equalsIgnoreCase("auth-int")) {
            hasha2 = DigestUtils.md5Hex(String.valueOf(HttpMethod) + ":" + uri + ":" + DigestUtils.md5Hex(input));
        }
        String hx_response = DigestUtils.md5Hex(String.valueOf(hasha1) + ":" + nonce + ":" + nc + ":" + cnonce + ":" + qop + ":" + hasha2);
        if (!qop.equalsIgnoreCase("auth-int") && !qop.equalsIgnoreCase("auth")) {
            hx_response = DigestUtils.md5Hex(String.valueOf(hasha1) + ":" + nonce + ":" + nc + ":" + cnonce + ":" + qop + ":" + hasha2);
        }
        conn = (HttpsURLConnection) url.openConnection();
        conn.setDoInput(true);
        conn.setDoOutput(true);
        conn.setRequestProperty("Authorization", "Digest username=\"" + HeaderUsername + "\", realm=\"" + realm + "\", nonce=\"" + nonce + "\", uri=\"" + uri + "\", algorithm=\"MD5\", qop=auth, nc=00000001, cnonce=\"" + cnonce + "\", response=\"" + hx_response + "\", opaque=\"" + opaque + "\"");
        printDebugMessage("Authorization , Digest username=\"" + HeaderUsername + "\", realm=\"" + realm + "\", nonce=\"" + nonce + "\", uri=\"" + uri + "\", algorithm=\"MD5\", qop=auth, nc=00000001, cnonce=\"" + cnonce + "\", response=\"" + hx_response + "\", opaque=\"" + opaque + "\"");
        printDebugMessage("WS Input Values = " + input);
        OutputStreamWriter streamWriter = new OutputStreamWriter(conn.getOutputStream());
        streamWriter.write(input);
        streamWriter.flush();
        conn.connect();
        return conn.getInputStream();
    }

    public static String process(HashMap<String, String> mp, HashMap<String, String> mr, SMSRequest request) throws Exception {
        String mensajeResponse = "";
        String debugProperty = mp.get("debug");
        if (debugProperty != null && debugProperty.equalsIgnoreCase("true")) {
            debugMode = true;
        }
        printDebugMessage("mp.debug : \"" + (String) mp.get("debug") + "\"");
        printDebugMessage("mp.HttpMethod : \"" + (String) mp.get("HttpMethod") + "\"");
        printDebugMessage("mp.endpoint : \"" + (String) mp.get("endpoint") + "\"");
        printDebugMessage("mp.uri : \"" + (String) mp.get("uri") + "\"");
        printDebugMessage("mp.HeaderUsername : \"" + (String) mp.get("HeaderUsername") + "\"");
        printDebugMessage("mp.HaderPassword : \"" + (String) mp.get("HaderPassword") + "\"");
        printDebugMessage("mp.regex_validation : \"" + (String) mp.get("regex_validation") + "\"");
        printDebugMessage("mr.mensaje_error : \"" + (String) mr.get("mensaje_error") + "\"");
        printDebugMessage("mr.mensaje_empty : \"" + (String) mr.get("mensaje_empty") + "\"");
        printDebugMessage("mr.mensaje_rejected : \"" + (String) mr.get("mensaje_rejected") + "\"");
        String msg_test = request.getTexto();
        String number = request.getNumero();
        printMessage("Recibiendo mensaje del " + number + " | " + msg_test);
        String codPais = number.substring(0, 2);
        String codArea = number.substring(2, 5);
        String numRem = number.substring(5);

            try {
                if (validarMensaje(msg_test, mp.get("regex_validation"))) {
                    String inputWS = "clave=mercurio"
                            + "&codPais=" + codPais
                            + "&codArea=" + codArea
                            + "&numRem=" + numRem
                            + "&mensaje=" + msg_test;
                    InputStream in = createRestRequest(mp.get("endpoint"),
                            mp.get("uri"),
                            mp.get("HttpMethod"),
                            mp.get("HeaderUsername"),
                            mp.get("HaderPassword"),
                            inputWS);
                    String response = "";
                    byte[] bodyByte = new byte[1024];
                    while (in.read(bodyByte) != -1) {
                        response = String.valueOf(response) + new String(bodyByte);
                    }
                    mensajeResponse = response.substring(response.indexOf("\"ResultMessage\":\"") + 17, response.indexOf("\"}"));
                    printDebugMessage("WS RESULT: " + response);
                    if (mensajeResponse.trim().isEmpty()) {
                        mensajeResponse = mr.get("mensaje_empty");
                    }
                    printMessage("Response Message: " + mensajeResponse);
                } else {
                    printMessage("Mensaje rechazado por REGEX " + (String) mr.get("mensaje_rejected"));
                    mensajeResponse = mr.get("mensaje_rejected");
                }
            } catch (Exception ex) {
                mensajeResponse = mr.get("mensaje_error");
                printMessage("BNC Exception: ");
                ex.printStackTrace();
            }
            return mensajeResponse;
        }
    }
